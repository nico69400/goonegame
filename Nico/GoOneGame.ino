/*
  JoystickMouseControl

  Controls the mouse from a joystick on an Arduino Leonardo, Micro or Due.
  Uses a pushbutton to turn on and off mouse control, and a second pushbutton
  to click the left mouse button.

  Hardware:
  - 2-axis joystick connected to pins A0 and A1
  - pushbuttons connected to pin D2 and D3

  The mouse movement is always relative. This sketch reads two analog inputs
  that range from 0 to 1023 (or less on either end) and translates them into
  ranges of -6 to 6.
  The sketch assumes that the joystick resting values are around the middle of
  the range, but that they vary within a threshold.

  WARNING: When you use the Mouse.move() command, the Arduino takes over your
  mouse! Make sure you have control before you use the command. This sketch
  includes a pushbutton to toggle the mouse control state, so you can turn on
  and off mouse control.

  created 15 Sep 2011
  updated 28 Mar 2012
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/JoystickMouseControl
*/

#include "Mouse.h"

// set pin numbers for switch, joystick axes, and LED:
const int switchPin = 2;      // switch to turn on and off mouse control
const int JoyStick_Left_Button = 9;    // input pin for the mouse pushButton
const int JoyStick_Left_X = A0;         // joystick X axis
const int JoyStick_Left_Y = A1;         // joystick Y axis
const int JoyStick_Right_Button = 8;    // input pin for the mouse pushButton
const int JoyStick_right_X = A2;         // joystick X axis
const int JoyStick_right_Y = A3;         // joystick Y axis
const int Croix_Dir_UP = 6;
const int Croix_Dir_LEFT = 5;
const int Croix_Dir_RIGHT = 7;
const int Croix_Dir_DOWN = 4;
const int Croix_Down = 15;
const int Carre_Left = 14;
const int Rond_Right = 10;
const int Triangle_Up = 16;

// parameters for reading the joystick:
int range = 12;               // output range of X or Y movement
int responseDelay = 5;        // response delay of the mouse, in ms
int threshold = range / 4;    // resting threshold
int center = range / 2;       // resting position value

bool mouseIsActive = true;    // whether or not to control the mouse
int lastSwitchState = LOW;        // previous switch state

void setup() {
  Serial.begin(115200);
  pinMode(switchPin, INPUT);       // the switch pin
  pinMode(Croix_Dir_UP, INPUT);
  pinMode(Croix_Dir_LEFT, INPUT);
  pinMode(Croix_Dir_RIGHT, INPUT);
  pinMode(Croix_Dir_DOWN, INPUT);
  pinMode(Croix_Down, INPUT);
  pinMode(Carre_Left, INPUT);
  pinMode(Rond_Right, INPUT);
  pinMode(Triangle_Up, INPUT);
  // take control of the mouse:
  Mouse.begin();
}
