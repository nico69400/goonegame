/*
  JoystickMouseControl

  Controls the mouse from a joystick on an Arduino Leonardo, Micro or Due.
  Uses a pushbutton to turn on and off mouse control, and a second pushbutton
  to click the left mouse button.

  Hardware:
  - 2-axis joystick connected to pins A0 and A1
  - pushbuttons connected to pin D2 and D3

  The mouse movement is always relative. This sketch reads two analog inputs
  that range from 0 to 1023 (or less on either end) and translates them into
  ranges of -6 to 6.
  The sketch assumes that the joystick resting values are around the middle of
  the range, but that they vary within a threshold.

  WARNING: When you use the Mouse.move() command, the Arduino takes over your
  mouse! Make sure you have control before you use the command. This sketch
  includes a pushbutton to toggle the mouse control state, so you can turn on
  and off mouse control.

  created 15 Sep 2011
  updated 28 Mar 2012
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/JoystickMouseControl
*/

#include "Mouse.h"

// set pin numbers for switch, joystick axes, and LED:
const int switchPin = 2;      // switch to turn on and off mouse control
const int JoyStick_Left_Button = 9;    // input pin for the mouse pushButton
const int JoyStick_Left_X = A0;         // joystick X axis
const int JoyStick_Left_Y = A1;         // joystick Y axis
const int JoyStick_Right_Button = 8;    // input pin for the mouse pushButton
const int JoyStick_right_X = A2;         // joystick X axis
const int JoyStick_right_Y = A3;         // joystick Y axis
const int Croix_Dir_UP = 6;
const int Croix_Dir_LEFT = 5;
const int Croix_Dir_RIGHT = 7;
const int Croix_Dir_DOWN = 4;
const int Croix_Down = 15;
const int Carre_Left = 14;
const int Rond_Right = 10;
const int Triangle_Up = 16;

// parameters for reading the joystick:
int range = 12;               // output range of X or Y movement
int responseDelay = 5;        // response delay of the mouse, in ms
int threshold = range / 4;    // resting threshold
int center = range / 2;       // resting position value

bool mouseIsActive = true;    // whether or not to control the mouse
int lastSwitchState = LOW;        // previous switch state

void setup() {
  Serial.begin(115200);
  pinMode(switchPin, INPUT);       // the switch pin
  pinMode(Croix_Dir_UP, INPUT);
  pinMode(Croix_Dir_LEFT, INPUT);
  pinMode(Croix_Dir_RIGHT, INPUT);
  pinMode(Croix_Dir_DOWN, INPUT);
  pinMode(Croix_Down, INPUT);
  pinMode(Carre_Left, INPUT);
  pinMode(Rond_Right, INPUT);
  pinMode(Triangle_Up, INPUT);
  // take control of the mouse:
  Mouse.begin();
}

void loop() {
  // read the switch:
  int switchState = digitalRead(switchPin);
  // if it's changed and it's high, toggle the mouse state:
//  if (switchState != lastSwitchState) {
//    if (switchState == HIGH) {
//      mouseIsActive = !mouseIsActive;
//      // turn on LED to indicate mouse state:
//      //digitalWrite(ledPin, mouseIsActive);
//      Serial.println(switchState);
//      Serial.println("pressed");
//    }
//  }
  // save switch state for next comparison:
  lastSwitchState = switchState;

  // read and scale the two axes:
  int JoyStick_Left_xReading = readAxis(A0);
  int JoyStick_Left_yReading = readAxis(A1);
  int JoyStick_right_xReading = readAxis(A2);
  int JoyStick_Right_yReading = readAxis(A3);
  Serial.print("Joystick Left: ");
  Serial.println(String(JoyStick_Left_xReading)+";"+String(JoyStick_Left_yReading));
  Serial.print("Joystick Right: ");
  Serial.println(String(JoyStick_right_xReading)+";"+String(JoyStick_Right_yReading));

  // if the mouse control state is active, move the mouse:
  if (mouseIsActive) {
    Mouse.move(JoyStick_Left_xReading, JoyStick_Left_yReading, 0);
  }

  // read the mouse button and click or not click:
  // if the mouse button is pressed:
  if (digitalRead(JoyStick_Left_Button) == LOW) {
    // if the mouse is not pressed, press it:
    if (!Mouse.isPressed(MOUSE_LEFT)) {
      Mouse.press(MOUSE_LEFT);
      Serial.println("pressed left");
    }
  }
  // else the mouse button is not pressed:
  else {
    // if the mouse is pressed, release it:
    if (Mouse.isPressed(MOUSE_LEFT)) {
      Mouse.release(MOUSE_LEFT);
      
    }
  }
  if (digitalRead(JoyStick_Right_Button) == LOW) {
    // if the mouse is not pressed, press it:
    if (!Mouse.isPressed(MOUSE_RIGHT)) {
      Mouse.press(MOUSE_RIGHT);
      Serial.println("pressed right");
    }
  }
  // else the mouse button is not pressed:
  else {
    // if the mouse is pressed, release it:
    if (Mouse.isPressed(MOUSE_RIGHT)) {
      Mouse.release(MOUSE_RIGHT);
      
    }
  }

  if (digitalRead(Croix_Dir_UP) == LOW) {
    Serial.println("Croix up pressed");
    
  }
  if (digitalRead(Croix_Dir_DOWN) == LOW) {
    Serial.println("Croix down pressed");
  }
  if (digitalRead(Croix_Dir_LEFT) == LOW) {
    Serial.println("Croix left pressed");
  }
  if (digitalRead(Croix_Dir_RIGHT) == LOW) {
    Serial.println("Croix right pressed");
  }

  if (digitalRead(Croix_Down) == LOW) {
    //Serial.println("CROIX pressed");
    Serial.print(F(" - Cross"));
  }
  if (digitalRead(Carre_Left) == LOW) {
    //Serial.println("CARRE pressed");
    Serial.print(F(" - Square"));
  }
  if (digitalRead(Rond_Right) == LOW) {
    //Serial.println("ROND pressed");
    Serial.print(F(" - Circle"));
  }
  if (digitalRead(Triangle_Up) == LOW) {
    //Serial.println("TRIANGLE pressed");
    Serial.print(F(" - Triangle"));
  }
  
  delay(responseDelay);
}

/*
  reads an axis (0 or 1 for x or y) and scales the analog input range to a range
  from 0 to <range>
*/

int readAxis(int thisAxis) {
  // read the analog input:
  int reading = analogRead(thisAxis);

  // map the reading from the analog input range to the output range:
  reading = map(reading, 0, 1023, 0, range);

  // if the output reading is outside from the rest position threshold, use it:
  int distance = reading - center;

  if (abs(distance) < threshold) {
    distance = 0;
  }

  // return the distance for this axis:
  return distance;
}